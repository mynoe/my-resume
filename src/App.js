import React from 'react';
import logo from './logo.svg';
import Header from './components/header'
import Resume from './components/resume'
import BackTop from './components/backTop'
import DownLoad from './components/downLoad'
import './App.css';

function App() {
  return (
    <div className="App" >
      <Header/>
      <Resume/>
      <BackTop/>
      <DownLoad/>
    </div>
  );
}

export default App;
