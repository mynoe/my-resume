import React from 'react'
import './index.sass'
import downLoad from '../../assets/download.png'
import tools from '../../utils/tools'
class DownLoad extends React.Component{
    constructor(props){
        super(props)
    }

    takeShot=()=>{
        tools.takeScreenShop()
    }


    render(){
        return(
            <div className='down-load-box' onClick={this.takeShot}>
                <div className='down-load-inner'>
                    <img src={downLoad} width={30}/>
                </div>
            </div>
        )
    }
}

export default DownLoad