import React from 'react'
import './index.sass'
import up1 from '../../assets/up1.png'
class BackTop extends React.Component{
    constructor(props){
        super(props)
        this.obtn = null
        this.timer = null;
        this.isTop = true;
        this.clientHeight =null;
        this.osTop = null; 
    }
    componentDidMount(){
        let btn = document.getElementById('btn')
        window.onscroll = function() {
            this.osTop = document.documentElement.scrollTop || document.body.scrollTop; //特别注意这句，忘了的话很容易出错
                //或者当页面的可视窗口高度clientHeight小于滚动条的高度时，出现返回顶部按钮
                if(this.osTop >= 100) {
                    btn.style.display = 'block';
                } else {
                    btn.style.display = 'none';
                }
    
                if(!this.isTop) {
                    clearInterval(this.timer);
                }
                this.isTop = false;
        }
    }

    backToTop=()=>{
        let self = this
        //设置定时器
        this.timer = setInterval(function() {
            //获取滚动条距离顶部的高度
            self.osTop = document.documentElement.scrollTop || document.body.scrollTop; //同时兼容了ie和Chrome浏览器
            //减小的速度
            var isSpeed = Math.floor(-self.osTop / 6);
            document.documentElement.scrollTop = document.body.scrollTop = self.osTop + isSpeed;
                        
            self.isTop = true;
            //判断，然后清除定时器
            if(self.osTop == 0) {
                clearInterval(self.timer);
            }
            }, 30);
    }

    render(){
        return(
            <div className='back-top' id="btn" ref='btn' onClick={this.backToTop}>
                <div className="back-top-inner">
                    <img src={up1} width={32}/>
                </div>
            </div>
        )
    }
}

export default BackTop