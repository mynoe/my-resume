import React from 'react'
import logo from '../../logo.svg';
import './index.sass'

class Header extends React.Component{


    render(){
        return(
            <div className="header">
                <div className='header-inner'>
                    <img src={logo} className="logo" width={120} height={120}/>
                </div>
                <div><p className="my-eng">Terrence</p></div>
            </div>
        )
    }
}
export default Header