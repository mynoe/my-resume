import React from 'react'
import './index.sass'
import BackTop from '../backTop'
class Resume extends React.Component{
    render(){
        return(
        <div className='resume-box' id='my-resume'>
           
            <section>
            <h1 className="f-s-18 f-w-900 m-b-20">个人信息</h1>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest p-t-0 p-b-0">
                <div className="row">
                  <div className="col-4 p-t-10 p-b-10">● 张超 | 男 | 1990</div>
                  <div className="col-4 p-t-10 p-b-10 border">● 本科(非统招) | 河南财经 </div>
                  <div className="col-4 p-t-10 p-b-10 border">● 前端开发 |  5年工作经验</div>
                </div>
                <div className="row">
                  <div className="col-4 p-t-10 p-b-10 border" >● 期望职位：Web前端开发</div>
                  <div className="col-4 p-t-10 p-b-10 border">● 期望城市：北京</div>
                  <div className="col-4 p-t-10 p-b-10 border"></div>
                </div>
              </div>
              {/* <div className="box bg-gray-lightest t-left">
                ● Github：
                <a href="https://terrence386.github.io/" target="_blank">https://terrence386.github.io/</a>
              </div>
              <div className="box bg-gray-lightest t-left">
                ● Gitee：
                <a href="https://gitee.com/mynoe/projects" target="_blank">https://gitee.com/mynoe/projects</a>
              </div> */}
              {/* <div className="box bg-gray-lightest t-left">
                ● 技术博客：
                <a href="https://www.redspite.com" target="_blank">https://www.redspite.com</a>
              </div> */}
            </div>
          </section>
          <section>
                <h1 className="f-s-18 f-w-900 m-b-20">联系方式</h1>
                <div className="boxes default m-b-20">
                <div className="box bg-gray-lightest p-t-0 p-b-0">
                    <div className="row">
                    <div className="col-4 p-t-10 p-b-10 border">● 手机：18638772351</div>
                    <div className="col-4 p-t-10 p-b-10 border">● Email：814324580@qq.com</div>
                    {/* <div className="col-4 p-t-10 p-b-10 border">● 网站：terrence386.github.io</div> */}
                    </div>
                </div>
                </div>
            </section>
          <section>
            <h1 className="f-s-18 f-w-900 m-b-20">技能掌握</h1>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 前端框架：熟练使用Vue/React/小程序开发
              </div>
              <div className="box bg-gray-lightest t-left">
                ● 前端工具：Webpack/Cordova/Git
              </div>
              <div className="box bg-gray-lightest t-left">
                ● 后端技能：熟悉node Linux Nginx 
              </div>
              <div className="box bg-gray-lightest t-left">
                ● 微信生态：公众号/小程序/企业微信开发
              </div>
            </div>
          </section>
          <section>
            <h1 className="f-s-18 f-w-900 m-b-20">工作经历</h1>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 北京明之智想数据科技有限公司 （ 2022 年 1 月 ~ 至今 ）
              </div>
              <div className="box bg-gray-lightest t-left">
                {/* <p className="m-t-0"><b>明智门店小程序开发</b></p> */}
                <p>- 明之门店小程序开发</p>
                <p>- 明之数据BI后台开发</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 北京讯锡科技 （ 2021 年 1 月 ~ 2022 年 1 月 ）
              </div>
              <div className="box bg-gray-lightest t-left">
                {/* <p className="m-t-0"><b>WebApp、微信小程序、考勤后台管理系统开发</b></p> */}
                <p>- 东东代理人分销后台/面客端/小程序开发</p>
                <p>- 企业微信相关自建应用开发</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 北京临高因爱科技有限公司（ 2020 年 3 月 ~ 2020 年 12 月 ）
              </div>
              <div className="box bg-gray-lightest t-left">
                {/* <p className="m-t-0"><b>后台管理系统开发</b> </p> */}
                <p> - 心动名片小程序开发</p>
                <p>- belove App 相关H5界面开发</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 北京美住美宿科技有限公司 （ 2019 年 9 月 ~ 2020 年 2 月 ）
              </div>
              <div className="box bg-gray-lightest t-left">
                {/* <p className="m-t-0"><b>后台管理系统开发</b> </p> */}
                <p> - 搭建npm私有仓库</p>
                <p>- 基于git hook搭建前端测试环境</p>
                <p>- 负责轻盈rms系统商家端小程序/总部pc端以及BI报表研发</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                ● 北京企新未来网络科技有限公司 2018 年 3 月 ~ 2019 年 9 月 ）
              </div>
              <div className="box bg-gray-lightest t-left">
                {/* <p className="m-t-0"><b>后台管理系统开发</b> </p> */}
                <p> - 劳勤云考勤后台/小程序/客户端开发</p>
              </div>
            </div>
            ......
          </section>
          <section>
            <h1 className="f-s-18 f-w-900 m-b-20">项目经验</h1>
            <div className="boxes default m-b-20">
              {/* <div className="box bg-gray-lightest t-left">
                代理人平台
              </div> */}
              <div className="box bg-gray-lightest t-left">
                <p className="m-t-0"><b>代理人平台</b></p>
                <p>- 目标为建设一个大的二级分销平台</p>
                <p>- 三端：pc , h5 , 小程序</p>
                <p>- 技术：pc采用vue, 小程序采用taro，组件化，配置化。</p>
                <p>- 结果：代理人平台基本成型，稳步迭代；技术上沉淀了一些业务组件，同时为其他类似多端开发相关的应提供了借鉴。</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                <p className="m-t-0"><b>Rms总部pc端开发</b></p>
                <p>- 为方便总部职能团队建立线上业务渠道，提高处理问题效率，降低出错概率。</p>
                <p>- 产品模块主要包括：酒店管理/Kpi管理/收益目标管理/消息管理等。</p>
                <p>- 技术上采用vue进行开发，封装常用模块，自定义指令做权限判断。</p>
                <p>- 一个月内完成开发，并根据实际业务作出相应调整。</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                <p className="m-t-0"><b>劳勤云考勤后台/小程序/app</b> </p>
                <p> - 为解中小企业考勤痛点，提高人事管理效率，降低企业用工成本。</p>
                <p>- 主要功能：人事管理/部门管理/薪酬管理/假期管理/考勤排班/请假审批...</p>
                <p>- 技术：管理后台采用react技术栈/小程序web-view 结合h5/app 采用Cordova进行打包</p>
                <p>- 结果：各项功能快速迭代/满足了公司日常工作需求。</p>
              </div>
            </div>
            <div className="boxes default m-b-20">
              <div className="box bg-gray-lightest t-left">
                <p className="m-t-0"><b>客服聊天系统</b> </p>
                <p> - 为方便客户在线咨询问题，解决重复性问题，网站接入客服系统。</p>
                <p>- 系统采用react进行开发，封装了聊天相关的常用组件，集成了环信sdk。</p>
                <p>- 为方便其他同学进行维护，后期采用vue进行了重构，保持原有功能统一。</p>
                <p>- 可作为模版移植到其他需要接入的网站中。</p>
              </div>
            </div>
            ......
          </section>
        </div>)
    }
}

export default Resume